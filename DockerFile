# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Add a volume pointing to /tmp
VOLUME /tmp

# The application's jar file
ARG JAR_FILE=target/kafka-producer-mockup-2.5.0.RELEASE.jar

# Add the application's jar to the container
ADD ${JAR_FILE} kafka-producer-mockup.jar

# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/kafka-producer-mockup.jar"]
